﻿
// Simple color library.

// Andrés Villalobos ^ andresalvivar@gmail.com ^ twitter.com/matnesis
// 2015/09/30 09:48 PM


using UnityEngine;
using System.Collections;


public class Palette : MonoBehaviour
{
	[Header("Gray")]
	public Color grayLite;
	public Color grayStrong;

	[Header("Red")]
	public Color redAlert;
	public Color redSoft;
}
