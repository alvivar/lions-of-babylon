﻿public class Layer
{
	public const string Ground = "Ground";
	public const string Player = "Player";
	public const string NPC = "NPC";
	public const string Conversation = "Conversation";
	public const string House = "House";
	public const string Bushes = "Bushes";
	public const string Fruits = "Fruits";

	public const string Enemy = "Enemy";
	public const string EnemyDetection = "EnemyDetection";
	public const string Ghost = "Ghost";
	public const string Checkpoint = "Checkpoint";
	public const string Score = "Score";

	public const string Player1 = "Player1";
	public const string Player2 = "Player2";
	public const string Player3 = "Player3";
	public const string Player4 = "Player4";

	public const string BulletPlayer1 = "BulletPlayer1";
	public const string BulletPlayer2 = "BulletPlayer2";
	public const string BulletPlayer3 = "BulletPlayer3";
	public const string BulletPlayer4 = "BulletPlayer4";
	public const string BulletEnemy = "BulletEnemy";
}
