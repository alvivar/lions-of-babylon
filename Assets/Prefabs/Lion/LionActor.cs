
// @matnesis
// 2015/10/23 09:39 PM


using UnityEngine;
using System.Collections;
using DG.Tweening;
using matnesis.TeaTime;


public class LionActor : MonoBehaviour
{
	[Header("Config")]
	public float gunDistance = 0.40f;
	public bool allowAttack = true;
	public bool allowDamage = true;

	[Header("Config")]
	public float currentHp = 1;
	public Vector2 currentDirection;
	public Transform currentBullet;

	[Header("Bullets Required")]
	public Transform simpleBullet;
	public Transform powerBullet;

	[Header("Particles")]
	public ParticleSystem bulletGunBlast;
	public ParticleSystem dustTrail;

	[Header("Sounds")]
	public AudioClip engineClip;
	public AudioClip gunBlastClip;
	public AudioClip deadClip;
	public AudioClip respawnClip;


	private float attackHoldDuration = 0;
	private bool isDead = false;

	// Required components
	private Motion2D motion;
	private Motion2DJump jump;
	private Motion2DAxis axis;
	private GamepadInControl gamepad;
	private Renderer render;
	private Collider2D collidr;
	private Animator animator;
	private AudioSource audio;


	void Start()
	{
		motion = GetComponent<Motion2D>();
		jump = GetComponent<Motion2DJump>();
		axis = GetComponent<Motion2DAxis>();
		render = GetComponentInChildren<Renderer>();
		collidr = GetComponent<Collider2D>();
		animator = GetComponentInChildren<Animator>();
		audio = GetComponent<AudioSource>();


		gamepad = GetComponent<GamepadInControl>();

		// BEWARE This is inverted
		gamepad.OnAttack += OnAttackReleaseHandler;
		// gamepad.OnAttackRelease += OnAttackHandler;
		gamepad.OnDash += OnDashHandler;
		gamepad.OnDashRelease += OnDashReleaseHandler;


		// currentDirection needs to be initialized based on the default
		// weapon direction
		currentDirection = new Vector2(0, 1);


		// Dust
		if (dustTrail)
		{
			ParticleSystem dustt = Instantiate(dustTrail, transform.position, Quaternion.identity) as ParticleSystem;
			dustt.transform.SetParent(transform);
			dustt.transform.localPosition = Vector3.zero;

			dustTrail = dustt;
			dustTrail.enableEmission = false;
		}


		// Sound during walking
		this.tt("WalkingSound").Add((ttHandler t) =>
		{
			audio.PlayOneShot(engineClip, 0.5f);
			t.WaitFor(engineClip.length * 0.9f);
		})
		.Repeat();
	}


	void Update()
	{
		// Axis sincronization

		Vector2 movement = gamepad.Movement;
		axis.horizontalAxis = movement.x;
		axis.verticalAxis = movement.y;


		// Last direction is clean
		if (movement.sqrMagnitude != 0)
		{
			currentDirection = new Vector2(axis.horizontalAxis, axis.verticalAxis);
			if (Mathf.Abs(currentDirection.x) > Mathf.Abs(currentDirection.y))
			{
				currentDirection.y = 0;
			}
			else
			{
				currentDirection.x = 0;
			}
			currentDirection = currentDirection.normalized;
		}


		// Animations + Inclination
		if (animator)
		{
			animator.SetInteger("Walking", motion.direction.sqrMagnitude == 0 ? 0 : 1);


			// Inclination
			if (currentDirection.x > 0)
				animator.transform.eulerAngles = new Vector3(0, 0, 270);

			if (currentDirection.x < 0)
				animator.transform.eulerAngles = new Vector3(0, 0, 90);

			if (currentDirection.y > 0)
				animator.transform.eulerAngles = new Vector3(0, 0, 0);

			if (currentDirection.y < 0)
				animator.transform.eulerAngles = new Vector3(0, 0, 180);
		}


		// Audio stuff
		if (motion.direction.sqrMagnitude == 0 || isDead)
		{
			audio.Stop();
			this.tt("WalkingSound").Stop();
		}
		else
		{
			this.tt("WalkingSound").Play();
		}
	}


	// Hold test
	void OnAttackHandler()
	{
		attackHoldDuration = Time.time;
	}


	// Fast screenshake
	public static void ScreenShake()
	{
		Sequence ts = DOTween.Sequence().SetLoops(2);
		ts.Append(DOTween.To(
		              () => LionGameContext.camera2D.hardOffset,
		              x => LionGameContext.camera2D.hardOffset = x,
		              Vector3.ClampMagnitude(Random.insideUnitSphere, 0.02f),
		              0.10f));
		ts.Append(DOTween.To(
		              () => LionGameContext.camera2D.hardOffset,
		              x => LionGameContext.camera2D.hardOffset = x,
		              Vector3.ClampMagnitude(Random.insideUnitSphere, 0.02f),
		              0.10f));
		ts.Append(DOTween.To(
		              () => LionGameContext.camera2D.hardOffset,
		              x => LionGameContext.camera2D.hardOffset = x,
		              Vector3.zero,
		              0.10f));
	}


	// Shoot the bullet based on the attack hold
	void OnAttackReleaseHandler()
	{
		if (!allowAttack)
			return;

		if (this.tt("OnAttackReleaseHandler").IsPlaying)
			return;


		// Bullet creation
		currentBullet = null;
		this.tt("OnAttackReleaseHandler").Add(() =>
		{
			// Strong bullet
			// We aren't ready for this bullet, yet
			if (false)//Time.time - attackHoldDuration > 2)
			{
				currentBullet = powerBullet.lpSpawn(transform.position, Quaternion.identity);
				currentBullet.GetComponent<BulletController>().damage = 2;
			}
			// Simple bullet
			else
			{
				currentBullet = simpleBullet.lpSpawn(transform.position, Quaternion.identity);
				currentBullet.GetComponent<BulletController>().damage = 1;
			}

			// Layer Posession Hardcoded
			currentBullet.gameObject.layer = gameObject.layer;
			if (currentBullet.gameObject.layer == LayerMask.NameToLayer(Layer.Player1))
				currentBullet.gameObject.layer = LayerMask.NameToLayer(Layer.BulletPlayer1);

			if (currentBullet.gameObject.layer == LayerMask.NameToLayer(Layer.Player2))
				currentBullet.gameObject.layer = LayerMask.NameToLayer(Layer.BulletPlayer2);

			if (currentBullet.gameObject.layer == LayerMask.NameToLayer(Layer.Player3))
				currentBullet.gameObject.layer = LayerMask.NameToLayer(Layer.BulletPlayer3);

			if (currentBullet.gameObject.layer == LayerMask.NameToLayer(Layer.Player4))
				currentBullet.gameObject.layer = LayerMask.NameToLayer(Layer.BulletPlayer4);


			// A little ahead
			currentBullet.GetComponent<BulletController>().creator = transform;
			currentBullet.GetComponent<Motion2D>().direction = currentDirection;
			currentBullet.GetComponent<Collider2D>().enabled = true;
			currentBullet.transform.position = currentBullet.transform.position + (Vector3)(currentDirection * gunDistance);

			// Particle
			ParticleSystem newparticle = null;
			if (bulletGunBlast)
				newparticle = Instantiate(bulletGunBlast, transform.position, Quaternion.identity) as ParticleSystem;


			// Sound effect
			if (gunBlastClip)
				LionGameContext.sounds.audioSounds.PlayOneShot(gunBlastClip);


			// Screenshake
			// LionGameContext.camera2D.ScreenShake();
			LionActor.ScreenShake();


			// A little knockback effect
			this.tt("KnockbackEffect").Add(() =>
			{
				motion.update = false;
				transform.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
				transform.GetComponent<Rigidbody2D>().AddForce(currentDirection * -1 * 100);
			})
			.Add(0.10f, (ttHandler t) =>
			{
				motion.update = true;
			})
			.Wait();


			// Bullet inclination
			Renderer ren = currentBullet.GetComponentInChildren<Renderer>();

			if (currentDirection.x > 0)
			{
				ren.transform.eulerAngles = new Vector3(0, 0, 270);
				newparticle.transform.eulerAngles = new Vector3(0, 0, 270);
			}

			if (currentDirection.x < 0)
			{
				ren.transform.eulerAngles = new Vector3(0, 0, 90);
				newparticle.transform.eulerAngles = new Vector3(0, 0, 90);
			}

			if (currentDirection.y > 0)
			{
				ren.transform.eulerAngles = new Vector3(0, 0, 0);
				newparticle.transform.eulerAngles = new Vector3(0, 0, 0);
			}

			if (currentDirection.y < 0)
			{
				ren.transform.eulerAngles = new Vector3(0, 0, 180);
				newparticle.transform.eulerAngles = new Vector3(0, 0, 180);
			}
		})
		.Add(0.50f).Loop((ttHandler t) =>
		{
			if (currentBullet.GetComponent<BulletController>().hasExplode)
			{
				currentBullet.GetComponent<BulletController>().hasExplode = false;
				t.Break();
			}

			t.WaitFor(0.10f);
		})
		.Wait();
	}


	float dashStamina = 1.2f;
	float currentDashStamina = 1.2f;

	// Dash? fast move?
	void OnDashHandler()
	{
		this.tt("reloadDashStamina").Stop();

		this.tt("holdingDash")
		.If(() => currentDashStamina > 0)
		.Add(() =>
		{
			motion.limitOverride = motion.magnitude;
			dustTrail.enableEmission = true;
			audio.pitch = 1.2f;
		})
		.Loop((ttHandler t) =>
		{
			currentDashStamina -= t.deltaTime;
			if (currentDashStamina <= 0)
				t.Break();

			if (!isDead && !this.tt("invinsibilityCloak").IsPlaying)
				render.material.color = Color.Lerp(Color.white * 0.5f, Color.white, currentDashStamina);
		})
		.Add(() =>
		{
			motion.limitOverride = 0;
			dustTrail.enableEmission = false;
			audio.pitch = 1;
		})
		.Wait();


		// this.tt("DashLimiter").Add(() =>
		// {
		// 	motion.limitOverride = motion.magnitude;
		// 	dustTrail.enableEmission = true;
		// 	audio.pitch = 1.2f;
		// })
		// .Add(1.2f, () =>
		// {
		// 	motion.limitOverride = 0;
		// 	audio.pitch = 1f;

		// 	if (!isDead)
		// 		render.material.color = Color.white * 0.50f;

		// 	dustTrail.enableEmission = false;
		// })
		// .Add(2.5f, () =>
		// {
		// 	if (!isDead)
		// 		render.material.color = Color.white;
		// })
		// .Wait();
	}


	void OnDashReleaseHandler()
	{
		this.tt("holdingDash").Stop();

		motion.limitOverride = 0;
		audio.pitch = 1f;
		dustTrail.enableEmission = false;


		this.tt("reloadDashStamina")
		.Add(0.10f)
		.If(() => !this.tt("holdingDash").IsPlaying && currentDashStamina < dashStamina)
		.Loop((ttHandler t) =>
		{
			currentDashStamina += t.deltaTime * 0.5f; // Recharge rate per second
			if (currentDashStamina >= dashStamina)
				t.Break();

			if (!isDead && !this.tt("invinsibilityCloak").IsPlaying)
				render.material.color = Color.Lerp(Color.white * 0.5f, Color.white, currentDashStamina / dashStamina);
		})
		.Wait().Repeat();
	}


	public void ApplyDamage(BulletController bc)
	{
		if (!allowDamage)
			return;


		// Health interchange
		currentHp -= bc.damage;

		// keep the reference of the creator of the bullet
		Transform bulletCreator = bc.creator;
		if (currentHp <= 0)
		{
			// add a point to my killer
			if (bulletCreator != null && !isDead)
			{
				LionData creator = bulletCreator.GetComponent<LionData>();
				if(creator != null){
					creator.score++;
				}
				
				bulletCreator = null;
			}

			// Die and try to comeback
			isDead = true;
			this.tt("DeadProcess").Add((ttHandler t) =>
			{
				render.material.color = Color.clear;
				collidr.enabled = false;
				gamepad.update = false;

				Sequence colorpunch = DOTween.Sequence();
				colorpunch.Append(render.material.DOColor(Color.red, 0.125f));
				colorpunch.Append(render.material.DOColor(Color.yellow, 0.125f));
				colorpunch.Append(render.material.DOColor(Color.clear, 0.05f));

				colorpunch.Insert(0, transform.DOScale(new Vector3(1.2f, 1.2f, 1), 0.30f));
				colorpunch.Insert(0, transform.DORotate(new Vector3(0, 0, Random.Range(0, 360)), 0.30f));

				t.WaitFor(colorpunch.WaitForCompletion());
			})
			.Add(() =>
			{
				// Dead sound
				if (deadClip)
				{
					this.tt("delayedDeadClip").Add(Random.Range(0, 0.20f), () =>
					{
						LionGameContext.sounds.audioSounds.PlayOneShot(deadClip);
					})
					.Wait();
				}


				// Camera OFF
				if (LionGameContext.camera2D.focusGroup.Contains(transform))
					LionGameContext.camera2D.focusGroup.Remove(transform);
			})
			.Add(1, () =>
			{
				// Respawn sound
				if (respawnClip)
					LionGameContext.sounds.audioSounds.PlayOneShot(respawnClip);


				// Camera ON
				if (!LionGameContext.camera2D.focusGroup.Contains(transform))
					LionGameContext.camera2D.focusGroup.Add(transform);


				render.material.color = Color.white;
				collidr.enabled = true;
				transform.localScale = new Vector3(1, 1, 1);
				transform.localEulerAngles = new Vector3(0, 0, 0);


				// The spawn position
				// Debug.Log("spawners length " + LionGameContext.spawnPoints.Length);
				Vector3 spawnpos = LionGameContext.spawnPoints[Random.Range(0, LionGameContext.spawnPoints.Length)].transform.position;
				spawnpos.z = 0;
				transform.position = spawnpos;


				// Alive again
				currentHp = 1;
				isDead = false;
				gamepad.update = true;


				// Invinsibility for a little
				this.tt("invinsibilityCloak").Add((ttHandler t) =>
				{
					allowAttack = false;
					allowDamage = false;

					Sequence blink = DOTween.Sequence().SetLoops(4);
					blink.Append(render.material.DOColor(Color.white * 0.2f, 0.25f));
					blink.Append(render.material.DOColor(Color.white * 0.8f, 0.25f));

					t.WaitFor(blink.WaitForCompletion());
				})
				.Add(() =>
				{
					allowAttack = true;
					allowDamage = true;
					render.material.DOColor(Color.white, 0.20f);
				})
				.Wait();
			})
			.Wait();
		}
	}
}
