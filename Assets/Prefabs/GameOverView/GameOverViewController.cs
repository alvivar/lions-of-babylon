﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using matnesis.TeaTime;
using System.Linq;
using System.Collections.Generic;

public class GameOverViewController : MonoBehaviour {

	public Button restartButton;

	public Button backToMenuButton;

	public Image[] winnerIcons;

	public Text[] scoreTexts;

	public Sprite[] goldTokens;

	public Image tokenImage;

	public Text funnyText;

	public Text scoreText;

	public string[] funnyTexts = 
	{
			"Great is the guilt of an unnecessary war.", 
		 	"We are going to have peace even if we have to fight for it.", 
		 	"The two most powerful warriors are patience and time.",
		 	"It is well that war is so terrible, otherwise we should grow too fond of it."
	};

	// Use this for initialization
	void Start () {

		InitView (LionGameContext.players);

		this.tt ("changeFunnyTextsRoutine").Add (4f, delegate(){

			int index = Random.Range(0,funnyTexts.Length);

			funnyText.text = funnyTexts[index];

		}).Repeat();
	
		restartButton.onClick.AddListener (delegate() {

			Application.LoadLevel (Application.loadedLevel);
			
			Destroy (this.gameObject);

		});

		backToMenuButton.onClick.AddListener (delegate() {

			Application.LoadLevel (1);
			
			Destroy (this.gameObject);

		});

	}

	void InitView (LionData[] players){

		foreach (var icon in winnerIcons) {
			icon.enabled = false;
		}

		foreach (var text in scoreTexts) {
			text.enabled = false;
		}

		// sort the players
		LionData[] winners = players.OrderByDescending(p => p.score).ToArray();
		LionData winner = winners.FirstOrDefault ();


		uint index = 0;
		foreach(LionData player in players){

			if(winner.playerNumber == player.playerNumber){
				tokenImage.rectTransform.SetPosX(winnerIcons[index].rectTransform.position.x);
			}

			// get the winner
			winnerIcons[index].sprite = player.imgSprite;
			winnerIcons[index].enabled = true;

			scoreTexts[index].text = player.score.ToString("00");
			scoreTexts[index].enabled = true;

			index++;

		}



		// get a random prize
		//tokenImage.sprite = goldTokens [Random.Range (0, goldTokens.Length)];
	}
}
