﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using matnesis.TeaTime;

public class LevelGUIController : MonoBehaviour {

	public Text timeText;

	public Text playerOneScore;
	public Text playerTwoScore;
	public Text playerThreeScore;
	public Text playerFourScore;

	// Use this for initialization
	void Start () {

		// hide the chronometer if we are in the mode 
		if (LevelManagerController.currentMode == 0) {
			timeText.enabled = false;
		} else {
			timeText.enabled = true;
			renderChronometer();
		}

		renderScores ();

	}

	void renderChronometer(){
		string minutes = "00";
		
		string seconds = "00";
		
		float secondsSinceStart = 0;
		
		this.tt ("updateTextTimeRoutine").Add (delegate(ttHandler h) {
			
			float timer = LionGameContext.LevelManager.roundDuration - secondsSinceStart;
			
			if(timer >= 0){
				
				minutes = Mathf.Floor(timer / 60).ToString("00");
				
				seconds = Mathf.Floor(timer % 60).ToString("00");
				
				timeText.text = minutes + " :" + seconds;
				
				secondsSinceStart++;
			}
			
		}).Add(1f).Repeat();
	}

	void renderScores(){

		LionData playerOne = LionGameContext.players.Where(p => p != null && p.playerNumber == 0).FirstOrDefault();
		LionData playerTwo = LionGameContext.players.Where(p => p != null && p.playerNumber == 1).FirstOrDefault();
		LionData playerThree = LionGameContext.players.Where(p => p != null && p.playerNumber == 2).FirstOrDefault();
		LionData playerFour = LionGameContext.players.Where(p => p != null && p.playerNumber == 3).FirstOrDefault();
		
		this.tt ("updateScoreTextRoutine").Add (0.25f, delegate(ttHandler obj) {

			playerOneScore.text = playerOne != null ? playerOne.score.ToString("00") : "--";
			playerTwoScore.text = playerTwo != null ? playerTwo.score.ToString("00") : "--";
			playerThreeScore.text = playerThree != null ? playerThree.score.ToString("00") : "--";
			playerFourScore.text = playerFour != null ? playerFour.score.ToString("00") : "--";
			
		}).Repeat();
	}
}
