﻿using UnityEngine;
using System.Collections;
using System.Linq;
using matnesis.TeaTime;

public class LevelManagerController : MonoBehaviour {

	public float roundDuration = 60f;

	public GameOverViewController gameOverView;

	public static int currentMode = 0; // 0: stocks mode 1: time mode

	public int killsToWin = 0;

	// Use this for initialization
	void Start () {

		startGameMode ();

	}

	public void startGameMode (){

		if (currentMode == 1) {
			this.tt("EndRoundRoutine").Add(roundDuration, delegate(){

				// get the winner
				LionData winner = LionGameContext.players.OrderByDescending(p => p.score).FirstOrDefault();

				if(winner != null){

					// show game over view
					ShowGameOverView(winner);

				}

			});
		}

		if (currentMode == 0) {

			this.tt("stocksGameRoutine").Add(0.2f, delegate(ttHandler h){

				if(LionGameContext.players.Length > 0){

					LionData winner = LionGameContext.players.Where(p => p != null && p.score >= killsToWin).FirstOrDefault();

					if(winner!=null){

						// show game over view
						ShowGameOverView(winner);

						this.tt ("stocksGameRoutine").Stop();

					}

				}

			}).Repeat();

		}

	}

	public void ShowGameOverView(LionData winner){

		GameObject go = (GameObject)GameObject.Instantiate (gameOverView.gameObject, Vector3.zero, Quaternion.identity);

	}

}
