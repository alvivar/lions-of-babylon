﻿
// @matnesis
// 2015/10/24 12:01 PM


using UnityEngine;
using System.Collections;
using matnesis.TeaTime;


public class BulletController : MonoBehaviour
{
	public bool hasExplode = false;
	public int damage = 1;
	public Transform creator; // The one that shoots

	[Header("Particles")]
	public ParticleSystem bulletExplosion;

	[Header("Sounds")]
	public AudioClip bulletExplosionClip;



	void OnTriggerEnter2D()
	{
		// Explode!
		this.tt("Explode+Recycle").Add(() =>
		{
			// Kill everything around
			Collider2D[] collidersAround = Physics2D.OverlapCircleAll(transform.position, 0.24f);
			foreach (Collider2D c in collidersAround)
			{
				// Ignore the creator
				if (c.transform == creator)
					continue;


				WallPieceController wallPiece = c.GetComponent<WallPieceController>();
				if (wallPiece)
					wallPiece.fakeDestroy();


				if (c.GetComponent<LionActor>())
					c.GetComponent<LionActor>().ApplyDamage(this);
			}

			// Explosion
			// #todo Cache
			if (bulletExplosion)
				Instantiate(bulletExplosion, transform.position, Quaternion.identity);

			if (bulletExplosionClip)
				LionGameContext.sounds.audioSounds.PlayOneShot(bulletExplosionClip);

			hasExplode = true;
			transform.position = Random.insideUnitSphere * 999;
			GetComponent<Motion2D>().direction = Vector3.zero;
		})
		.Add(3, () =>
		{
			hasExplode = false;
			transform.lpRecycle();
			GetComponent<Collider2D>().enabled = false;
		})
		.Wait();
	}

}
