﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class IntroViewController : MonoBehaviour
{
	public Button startButton;


	void Start()
	{
		startButton.onClick.AddListener(delegate()
		{
			Application.LoadLevel(1);
		});
	}
}
