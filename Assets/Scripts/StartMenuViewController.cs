﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System.Collections.Generic;


public class StartMenuViewController : MonoBehaviour
{

	public Button stockModeButton;

	public Button timeModeButton;

	public Button backButton;

	// Use this for initialization
	void Start()
	{

		stockModeButton.onClick.AddListener(delegate()
		{

			LevelManagerController.currentMode = 0;

			int[] playableScenes = {3, 4, 5, 6, 7};

			int randomIndex = playableScenes[Random.Range(0, playableScenes.Length)];

			Application.LoadLevel(randomIndex);

		});


		timeModeButton.onClick.AddListener(delegate()
		{

			LevelManagerController.currentMode = 1;

			int[] playableScenes = {3, 4, 5, 6, 7};

			int randomIndex = playableScenes[Random.Range(0, playableScenes.Length)];

			Application.LoadLevel(randomIndex);

		});


		backButton.onClick.AddListener(delegate()
		{
			Application.LoadLevel(0);
		});
	}
}
