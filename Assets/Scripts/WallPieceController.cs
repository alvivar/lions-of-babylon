﻿
using UnityEngine;
using matnesis.TeaTime;
using System.Collections;


public class WallPieceController : MonoBehaviour
{
	public float ghostDuration = 2f;
	public AudioClip[] wallBreakClip;

	SpriteRenderer spriteRenderer;
	BoxCollider2D collider;


	void Start()
	{
		spriteRenderer = gameObject.GetComponent<SpriteRenderer> ();
		collider = gameObject.GetComponent<BoxCollider2D>();
	}


	public void fakeDestroy()
	{
		toggleTile(false);

		this.tt("ghostTileRoutine").Add(ghostDuration + Random.Range(0.10f, 0.40f), delegate(ttHandler obj)
		{
			toggleTile(true);
		})
		.Wait();

		if (wallBreakClip.Length > 0)
			LionGameContext.sounds.audioSounds.PlayOneShot(wallBreakClip[Random.Range(0, wallBreakClip.Length)], 0.20f);
	}


	void toggleTile(bool toggle)
	{
		spriteRenderer.enabled = toggle;
		collider.enabled = toggle;


		if (toggle)
		{
			spriteRenderer.color = new Color(1, 1, 1, 0);

			this.tt("tileFadeInRoutine").Loop(1f, delegate(ttHandler handler)
			{
				spriteRenderer.color = new Color(1, 1, 1, handler.t);
			})
			.Wait();
		}
	}
}
