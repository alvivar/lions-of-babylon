﻿using UnityEngine;
using System.Collections;
using matnesis.TeaTime;

public class TurretController : MonoBehaviour {

	public Transform bullet;

	public float bulletForce = 10f;

	public Vector2 currentDirection;

	public Transform currentBullet;

	public float gunDistance = 0.40f;

	// Use this for initialization
	void Start () {

		// Bullet creation
		currentBullet = null;

		this.tt("OnAttackReleaseHandler").Add(3f, delegate(ttHandler obj) {

			currentBullet = bullet.lpSpawn(transform.position, Quaternion.identity);

			currentBullet.gameObject.layer = LayerMask.NameToLayer(Layer.BulletEnemy);

			currentBullet.GetComponent<BulletController>().damage = 1;
			
			// Layer Posession
			//currentBullet.gameObject.layer = gameObject.layer;
			
			// A little ahead
			currentBullet.GetComponent<BulletController>().creator = transform;
			currentBullet.GetComponent<Motion2D>().direction = currentDirection;
			currentBullet.GetComponent<Collider2D>().enabled = true;
			currentBullet.transform.position = currentBullet.transform.position + (Vector3)(currentDirection * gunDistance);
			
			
			// Bullet inclination
			Renderer ren = currentBullet.GetComponentInChildren<Renderer>();
			
			if (currentDirection.x > 0)
				ren.transform.eulerAngles = new Vector3(0, 0, 270);
			
			if (currentDirection.x < 0)
				ren.transform.eulerAngles = new Vector3(0, 0, 90);
			
			if (currentDirection.y > 0)
				ren.transform.eulerAngles = new Vector3(0, 0, 0);
			
			if (currentDirection.y < 0)
				ren.transform.eulerAngles = new Vector3(0, 0, 180);
			
		}).Loop((ttHandler t) => {

			if (currentBullet.GetComponent<BulletController>().hasExplode)
			{
				currentBullet.GetComponent<BulletController>().hasExplode = false;
				t.Break();
			}
			
			t.WaitFor(0.10f);

		}).Repeat();

	}
}
